#ifndef _linkedlist_h
#define _linkedlist_h

#include "minutia.h"
#include "mTriplet.h"

/* Generic list */
struct node {
   void *info;
   struct node *next;
};
typedef struct node Node;

Node* initialize ()
{
	return 0;
}

Node* createMTriplet (Minutia m0, Minutia m1, Minutia m2)
{
	MTriplet* mTriplet;
	Node* node;
	// aloca MTriplet
	mTriplet = (MTriplet*) malloc(sizeof(MTriplet));
	mTriplet->m[0] = m0;
	mTriplet->m[1] = m1;
	mTriplet->m[2] = m2;
	// aloca no
	node = (Node*) malloc(sizeof(Node));
	node->info = mTriplet;
	node->next = 0;
	return node;
}

Node* insert (Node* firstNode, Node* newNode)
{
	if (firstNode != 0) {
		newNode->next = firstNode->next;
	    //printf("new node.next.next %p \n", newNode->next->next);
		firstNode->next = newNode;
		return firstNode;
	}
	firstNode = (Node*) malloc(sizeof(Node));
	newNode->next = 0;
    printf("newNode->next %p \n", newNode->next);
	firstNode->next = newNode;
    printf("firstNode->next->next %p \n", firstNode->next->next);
	return firstNode;
}

/* funcao imprime: imprime valores dos elementos */
void imprime (Node* l)
{
	Node* p;   /* variavel auxiliar para percorrer a lista */
	for (p = l; p != 0; p = p->next)
		printf("info = %d\n", p->info);
}

/* funcao vazia: retorna 1 se vazia ou 0 se nao vazia */
int isEmpty (Node* l)
{
	if (l == 0)
		return 1;
	else
		return 0;
}

#endif
