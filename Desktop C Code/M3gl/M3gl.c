#include <math.h>
#include <stdio.h>
#include "linkedlist.h"
#include "mtriplet.h"
#include "minutia.h"


#define tl (12)
#define tg (12)
#define ta (30)
#define c (4)
#define qms_size (22)
#define tms_size (26)
#define num_triangles1 (86)
#define num_triangles2 (97)
// ----------------------------------------
//           Global Variables
// ----------------------------------------
Node* firstNodeOfQueryMTripletLinkedList;
Node* firstNodeOfTemplateMTripletLinkedList;
char buffer[50];
// ----------------------------------------
//         Auxiliary Functions
// ----------------------------------------
float computeAngle(float dX, float dY) {
    if (dX > 0 && dY >= 0)
        return atan(dY / dX);
    if (dX > 0 && dY < 0)
        return atan(dY / dX) + 2 * M_PI;
    if (dX < 0)
        return atan(dY / dX) + M_PI;
    if (dX == 0 && dY > 0)
        return M_PI / 2;
    else //if (dX == 0 && dY < 0)
        return 3 * M_PI / 2;
}

/* Angular distance between two angles */
int adpi(int a1, int a2)
{
    int ad1, ad2;               // angular distances
    ad1 = fabs(a1 - a2);
    ad2 = 360 - fabs(a1 - a2);
    if (ad1 < ad2)
        return ad1;
    else return ad2;
}

/* Angular distance between two angles in clockwise sense */
int ad2pi(int a1, int a2)
{
    if (a2 >= a1)
        return a2 - a1;
    else return a2 - a1 + 360;
}

/**
 * Sort in ascending order
 */
void sortAscending(float *a, int n)
{
 float t;

  for (int j=0 ; j<(n-1) ; j++) {
    for (int i=0 ; i<(n-1) ; i++) {
      if (a[i+1] < a[i]) {
        t = a[i];
        a[i] = a[i + 1];
        a[i + 1] = t;
      }
    }
  }
}

/**
 * Converts from radians to degree
 */
float to_degrees(float radians) {
    return radians * (180.0 / M_PI);
}

/**
 * Two m-triplets are dissimilar if their minutiae directions differ more than π/4
 */
int so(MTriplet m1, MTriplet m2)
{
    if (adpi(m1.m[0].o, m2.m[0].o) > 45 || adpi(m1.m[1].o, m2.m[1].o) > 45 || adpi(m1.m[2].o, m2.m[2].o) > 45)
        return 0;

    return 1;
}

/**
 * Compares m-triplets in terms of the side lengths of the triangle formed by the triplet minutiae
 */
float sd(MTriplet m1, MTriplet m2)
{
    if (fabs(m1.d[0] - m2.d[0]) > tl || fabs(m1.d[1] - m2.d[1]) > tl || fabs(m1.d[2] - m2.d[2]) > tl)
        return 0;

    float max = 0;
    float aux;
    for (int i = 0; i < 3; i++)
    {
        aux = fabs(m1.d[i] - m2.d[i]);
        if (max < aux)
            max = aux;
    }
    //printf("max = %f\n", max);
    return 1 - max / tl;
}

/**
 * Compares m-triplets based on the angles formed by minutiae directions and the sides of the triangles (angles α)
 */
float sa(struct mTriplet m1, struct mTriplet m2)
{
    float max = 0;
    for (int i = 0; i < 6; i++)
    {
        if (adpi(m1.a[i], m2.a[i]) > ta)
        {
            //printf("Diferença alpha = %d\n", adpi(m1.a[i], m2.a[i]));
            return 0;
        }
        int angle = adpi(m1.a[i], m2.a[i]);
        if (angle > max)
        {
            max = angle;
        }
    }
    float result = 1 - max / ta;
    //printf ("Valor de sa %f\n", result);
    return result;
}

/**
 * Compares m-triplets based on relative minutiae directions (angles β)
 */
float sb(struct mTriplet m1, struct mTriplet m2)
{
    int max = 0;
    for (int i = 0; i < 3; i++)
    {
        int angle = adpi(m1.b[i], m2.b[i]);
        if (angle > ta)
        {
            return 0;
        }
        if (angle > max)
        {
            max = angle;
        }
    }
    return 1 - (float)max / (float)ta;
}

/* Euclidean distance between two points */
float ed(int x1, int y1, int x2, int y2)
{
    float difx = x1 - x2;
    float dify = y1 - y2;
    return sqrt(difx * difx + dify * dify);
}

/* Calculates the alpha values for a given minutia */
void calculateAlphas(MTriplet* mTriplet)
{
    int counter = 0;
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            if (i != j) {
                float dx = mTriplet->m[i].x - mTriplet->m[j].x;
                float dy = mTriplet->m[i].y - mTriplet->m[j].y;
                float lineAngle = computeAngle(dx, dy) * 57.2958;
                mTriplet->a[counter] = ad2pi(mTriplet->m[i].o, lineAngle);
                counter++;
            }
        }
    }
    //printf("(%d,%d,%d) %d | %d | %d | %d | %d | %d\n", mTriplet->m[0].id, mTriplet->m[1].id, mTriplet->m[2].id, mTriplet->a[0], mTriplet->a[1], mTriplet->a[2], mTriplet->a[3], mTriplet->a[4], mTriplet->a[5]);
}

/* Calculates the beta values for a given minutia */
void calculateBetas(MTriplet* m1)
{
    m1->b[0] = ad2pi(m1->m[0].o, m1->m[1].o);
    m1->b[1] = ad2pi(m1->m[1].o, m1->m[2].o);
    m1->b[2] = ad2pi(m1->m[2].o, m1->m[0].o);
}

/**
 * Shifts an m-Triplet in clockwise sense
 */
MTriplet shift(MTriplet m1)
{
    //printf("before rotation: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    struct minutia m = m1.m[0];
    m1.m[0] = m1.m[1];
    m1.m[1] = m1.m[2];
    m1.m[2] = m;
    //printf("after rotation: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    return m1;
}

/**
 * Base similarity function
 */
float sPart(MTriplet m1, MTriplet m2)
{
    if (so(m1, m2) == 0 || sd(m1, m2) == 0 || sa(m1, m2) == 0 || sb(m1, m2) == 0) {
        return 0;
    }

    //printf("____________________________________________________\n");
    //printf("m1: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    //printf("m2: %d | %d | %d\n", m2.m[0].id, m2.m[1].id, m2.m[2].id);
    //printf("m1 d: %f | %f | %f\n", m1.d[0], m1.d[1], m1.d[2]);
    //printf("sPart: %d | %f | %f | %f\n", so(m1, m2), sd(m1, m2), sa(m1, m2), sb(m1, m2));
    //printf("Diffs sd: %f | %f | %f\n", fabs(m1.d[0] - m2.d[0]), fabs(m1.d[1] - m2.d[1]), fabs(m1.d[2] - m2.d[2]));
    return 1 - (1 - sd(m1, m2)) * (1 - sa(m1, m2)) * (1 - sb(m1, m2));
}

void rebuildTriplet(MTriplet* mTriplet) {
    mTriplet->d[0] = ed(mTriplet->m[0].x, mTriplet->m[0].y, mTriplet->m[1].x, mTriplet->m[1].y);
    mTriplet->d[1] = ed(mTriplet->m[1].x, mTriplet->m[1].y, mTriplet->m[2].x, mTriplet->m[2].y);
    mTriplet->d[2] = ed(mTriplet->m[2].x, mTriplet->m[2].y, mTriplet->m[0].x, mTriplet->m[0].y);
    calculateAlphas(mTriplet);
    calculateBetas(mTriplet);
}

/**
 * M-triplets similarity
 * Accurately distinguish between similar and non-similar m-triplets
 */
float sInv(MTriplet m1, MTriplet m2, MTriplet* newTriplet)
{
  float m1distances[3];
  float m2distances[3];

  m1distances[0] = m1.d[0];
  m1distances[1] = m1.d[1];
  m1distances[2] = m1.d[2];
  m2distances[0] = m2.d[0];
  m2distances[1] = m2.d[1];
  m2distances[2] = m2.d[2];

  sortAscending(m1distances, 3);
  sortAscending(m2distances, 3);

  if ((fabs(m1distances[0] - m2distances[0]) > tl) || (fabs(m1distances[1] - m2distances[1]) > tl) || (fabs(m1distances[2] - m2distances[2]) > tl)) {
    return 0;
  }

    float maximum = 0;
    float aux;
    MTriplet newm2;
    maximum = sPart(m1, m2);
    newm2 = shift(m2);

    rebuildTriplet(&newm2);

    aux = sPart(m1, newm2);
    if (aux > maximum)
    {
        maximum = aux;
        m2 = newm2;
        *newTriplet = newm2;
    }
    newm2 = shift(newm2);

    rebuildTriplet(&newm2);

    aux = sPart(m1, newm2);
    if (aux > maximum)
    {
        maximum = aux;
        m2 = newm2;
        *newTriplet = newm2;
    }

    return maximum;
}

int lmm(Minutia qms[], Minutia tms[], Minutia mmp[][2])
{
  MTriplet localMatchingMTripletsPairs[35][2];  // atencao fixo
  float similarities[35];                       // atencao fixo
  int mmtpSize = 0;
  Node* auxNodeQ = (Node*) malloc(sizeof(Node));
  Node* auxNodeT = (Node*) malloc(sizeof(Node));
  auxNodeQ = firstNodeOfQueryMTripletLinkedList;
  // For each query m-triplet perform binary search looking for the template m-triplets with similarity value higher than 0
  printf("\nPoint B\n");
  while (auxNodeQ->next != 0) {
    auxNodeQ = auxNodeQ->next;
    MTriplet* queryTriplet = (MTriplet*) auxNodeQ->info;
    auxNodeT = firstNodeOfTemplateMTripletLinkedList;
    //println(ESP.getFreeHeap());
    while (auxNodeT->next != 0) {
      auxNodeT = auxNodeT->next;
      MTriplet* templateTriplet = (MTriplet*) auxNodeT->info;

      float similarity = 0;
      similarity = sInv(*queryTriplet, *templateTriplet, templateTriplet);
      //printf("Similarity - %f\n", similarity);
      if (similarity > 0)
      {
        printf("mmtpSize - %d\n", mmtpSize);
        localMatchingMTripletsPairs[mmtpSize][0] = *queryTriplet;
        localMatchingMTripletsPairs[mmtpSize][1] = *templateTriplet;
        similarities[mmtpSize] = similarity;

        mmtpSize++;
      }
    }
  }
    // Sort in descendant order all matching pairs (qts, tts) in mmtp according to the similarity value
    printf("mmtpSize = %d\n", mmtpSize);
    int tmp;
    MTriplet tmpmmtp[2];
    for (int i = 0; i < mmtpSize; i++)
    {
        for (int j = 0; j < mmtpSize-i; j++)
        {
            if (similarities[j] < similarities[j+1])
            {
                tmp = similarities[j+1];
                similarities[j+1] = similarities[j];
                similarities[j] = tmp;
                tmpmmtp[0] = localMatchingMTripletsPairs[j+1][0];
                tmpmmtp[1] = localMatchingMTripletsPairs[j+1][1];
                localMatchingMTripletsPairs[j+1][0] = localMatchingMTripletsPairs[j][0];
                localMatchingMTripletsPairs[j+1][1] = localMatchingMTripletsPairs[j][1];
                localMatchingMTripletsPairs[j][0] = tmpmmtp[0];
                localMatchingMTripletsPairs[j][1] = tmpmmtp[1];
            }
        }
    }
    printf("mmtpSize = %d\n", mmtpSize);
    // print all matching pairs
    printf("\nMatching triplet pairs");
    for (int j = 0; j < mmtpSize; j++)
    {
      printf("(%d,%d,%d)(%d,%d,%d)\n", localMatchingMTripletsPairs[j][0].m[0].id, localMatchingMTripletsPairs[j][0].m[1].id, localMatchingMTripletsPairs[j][0].m[2].id, localMatchingMTripletsPairs[j][1].m[0].id, localMatchingMTripletsPairs[j][1].m[1].id, localMatchingMTripletsPairs[j][1].m[2].id);
    }
    MTriplet mt0, mt1;
    int mmpcounter = 0;

    printf("\nMinutiae pairs");
    for (int i = 0; i < mmtpSize; i++)
    {
        int containsMinutia1 = 0;
        int containsMinutia2 = 0;
        mt0 = localMatchingMTripletsPairs[i][0];
        mt1 = localMatchingMTripletsPairs[i][1];
        for (int j = 0; j < 3; j++)
        {
          containsMinutia1 = 0;
          containsMinutia2 = 0;
            for (int k = 0; k < mmpcounter; k++)
            {
                if (mt0.m[j].x == mmp[k][0].x && mt0.m[j].y == mmp[k][0].y)
                {
                    containsMinutia1 = 1;
                }
                if (mt1.m[j].x == mmp[k][1].x && mt1.m[j].y == mmp[k][1].y)
                {
                    containsMinutia2 = 1;
                }
            }
            if (!containsMinutia1 || !containsMinutia2)
            {
                printf("(%d,%d)(%d,%d)\n", mt0.m[j].x, mt0.m[j].y, mt1.m[j].x, mt1.m[j].y);
                mmp[mmpcounter][0] = mt0.m[j];
                mmp[mmpcounter][1] = mt1.m[j];
                mmpcounter++;
            }
        }
    }

    return mmpcounter;
}

/**
 * Global Minutiae Matching
 * This step uses every minutiae pair as a reference pair for fingerprint rotation and performs a query
 * minutiae transformation for each reference pair. Later, it selects the transformation that maximizes
 * the amount of matching minutiae.
 * Returns the maximum number of matching minutiae computed after performing all fingerprint rotations.
 */
int gmm(Minutia mmp[][2], int mmpsize)
{
    int n = 0;      // maximum number of matching minutiae
    Minutia bestE[mmpsize][2];
    printf("\nGlobal Minutiae Matching");
    for (int i = 0; i < mmpsize; i++)
    {
        //printf("RefPair (%d,%d)(%d,%d)\n", mmp[i][0].x, mmp[i][0].y, mmp[i][1].x, mmp[i][1].y);
        Minutia E[mmpsize][2];
        int sizeE = 0;

        for (int j = 0; j < mmpsize; j++)
        {
            int contains = 0;
            for (int k = 0; k <= sizeE; k++)
            {
                if (((mmp[j][0].x == E[k][0].x) && (mmp[j][0].y == E[k][0].y)) || ((mmp[j][1].x == E[k][1].x) && (mmp[j][1].y == E[k][1].y)))
                {
                    contains = 1;
                }
            }
            if (contains == 0)
            {
                Minutia ql;
                int dAngle = mmp[i][1].o - mmp[i][0].o;
                int newAngle = mmp[j][0].o + dAngle;
                float seno = sin(dAngle / 57.2958);
                float coss = cos(dAngle / 57.2958);
                //printf("    Cosseno: %f", coss);
                ql.o = (newAngle > 360) ? newAngle - 360 : (newAngle < 0) ? newAngle + 360 : newAngle;
                ql.x = (mmp[j][0].x - mmp[i][0].x) * coss - (mmp[j][0].y - mmp[i][0].y) * seno + mmp[i][1].x;
                ql.y = (mmp[j][0].x - mmp[i][0].x) * seno + (mmp[j][0].y - mmp[i][0].y) * coss + mmp[i][1].y;
                //ql.x = cos(dAngle) * (mmp[j][0].x - mmp[i][0].x) - sin(dAngle) * (mmp[j][0].y - mmp[i][0].y) + mmp[i][1].x;
                //ql.y = sin(dAngle) * (mmp[j][0].x - mmp[i][0].x) + cos(dAngle) * (mmp[j][0].y - mmp[i][0].y) + mmp[i][1].y;
                //printf("    Pair (%d,%d)(%d,%d)\n", mmp[j][0].x, mmp[j][0].y, mmp[j][1].x, mmp[j][1].y);
                //printf("    mAngle %f\n", mmp[j][0].o);
                //printf("    dAngle %d\n", dAngle);
                //printf("    newAngle %d\n", newAngle);
                //printf("    Mapped minutia: (%d,%d)>%f\n", ql.x, ql.y, ql.o);
                //Serial.println("---------------------");
                if (fabs(ed(ql.x, ql.y, mmp[j][1].x, mmp[j][1].y)) <= tg && adpi(ad2pi(mmp[i][1].o, mmp[i][0].o), ad2pi(mmp[j][1].o, mmp[j][0].o)) <= ta && adpi(ql.o, mmp[j][1].o) <= ta)
                {
                    E[sizeE][0] = mmp[j][0];
                    E[sizeE][1] = mmp[j][1];
                    sizeE++;
                }
            }
        }
        if (n < sizeE)
        {
            n = sizeE;
            for (int w = 0; w < n; w++)
            {
              bestE[w][0] = E[w][0];
              bestE[w][1] = E[w][1];
            }
        }
    }
    printf("\nFINAL RESULT\n");
    for (int w = 0; w < n; w++)
    {
      printf("(%d,%d)(%d,%d)\n", bestE[w][0].x, bestE[w][0].y, bestE[w][1].x, bestE[w][1].y);
    }
    return n;
}

/**
 *  Similarity Score Computation
 */
float ssc(float n)
{
    return (n*n)/(qms_size * tms_size);
}

void initializeMinutiaeSets(Minutia qms[], Minutia tms[]) {
  //105_3
  float minutiae1[qms_size][3] = {
              {187,165,2.94},
              {235,173,0.09},
              {321,162,0.61},
              {236,207,0.46},
              {185,223,2.94},
              {195,208,0},
              {259,214,3.68},
              {172,234,5.81},
              {198,237,0},
              {306,237,1.10},
              {163,268,5.81},
              {140,285,5.25},
              {191,285,3.33},
              {204,283,0.83},
              {215,285,4.24},
              {172,288,2.18},
              {172,297,1.86},
              {181,292,1.27},
              {187,288,0.96},
              {176,307,1.57},
              {197,312,4.51},
              {128,367,5.25}
  };
  //105_6
  float minutiae2[tms_size][3] = {
              {310,158,3.68},
              {320,156,3.81},
              {192,170,3.43},
              {315,171,0.78},
              {316,160,0.67},
              {192,218,0.46},
              {168,236,6.18},
              {182,228,3.14},
              {233,226,0.89},
              {242,230,3.87},
              {324,230,0.7},
              {291,252,0.85},
              {144,271,5.99},
              {160,272,0.09},
              {184,280,3.68},
              {164,290,1.03},
              {174,299,1.03},
              {187,288,0.96},
              {196,303,4.24},
              {207,296,0.96},
              {199,314,1.27},
              {323,309,1.19},
              {194,324,1.57},
              {178,359,1.86},
              {272,379,4.03},
              {310,377,3.52}
  };

  int i;
  // Initialize query minutiae
  for(i=0; i<qms_size; i++) {
    qms[i].id = i;
    qms[i].x = minutiae1[i][0];
    qms[i].y = minutiae1[i][1];
    qms[i].o = minutiae1[i][2] * 57.2958;
  }

  // Initialize template minutiae
  for(i=0; i<tms_size; i++) {
    tms[i].id = i;
    tms[i].x = minutiae2[i][0];
    tms[i].y = minutiae2[i][1];
    tms[i].o = minutiae2[i][2] * 57.2958;
  }
}

int getTriangle1ValueAt(int line, int column) {
  int triangles1[num_triangles1][3] = {
    {16,15,17},
    {17,12,18},
    {17,15,18},
    {19,16,17},
    {16,18,17},
    {16,15,18},
    {18,12,13},
    {18,15,12},
    {17,15,12},
    {19,16,15},
    {19,15,17},
    {19,16,18},
    {16,12,18},
    {16,15,12},
    {16,12,17},
    {14,12,13},
    {17,12,13},
    {17,18,13},
    {20,19,17},
    {20,19,18},
    {20,17,18},
    {8,7,4},
    {20,17,12},
    {20,19,12},
    {20,18,12},
    {18,13,14},
    {18,12,14},
    {8,4,5},
    {20,19,16},
    {17,15,10},
    {17,16,10},
    {15,16,10},
    {15,11,10},
    {15,12,13},
    {20,12,14},
    {20,18,14},
    {20,13,14},
    {16,11,10},
    {16,11,15},
    {20,19,15},
    {8,7,5},
    {17,13,14},
    {4,7,5},
    {17,11,15},
    {17,16,11},
    {17,11,10},
    {10,7,8},
    {6,3,1},
    {8,5,3},
    {10,4,8},
    {10,7,4},
    {3,5,1},
    {5,0,1},
    {4,5,3},
    {5,4,0},
    {13,7,8},
    {13,4,8},
    {6,5,3},
    {6,5,1},
    {3,5,0},
    {3,4,0},
    {3,0,1},
    {8,3,6},
    {10,7,5},
    {10,4,5},
    {7,5,3},
    {4,0,1},
    {8,1,3},
    {13,8,5},
    {9,3,6},
    {9,6,2},
    {21,16,19},
    {21,11,16},
    {21,11,19},
    {6,1,2},
    {6,0,1},
    {21,11,20},
    {21,19,20},
    {21,16,20},
    {9,3,1},
    {9,1,2},
    {9,6,1},
    {3,1,2},
    {6,3,2},
    {9,3,2},
    {9,5,6}
  };
  return triangles1[line][column];
}

int getTriangle2ValueAt(int line, int column) {
  int triangles2[num_triangles2][3] = {
    {4,0,1},
    {3,0,4},
    {3,4,1},
    {3,0,1},
    {20,18,19},
    {20,22,18},
    {16,14,17},
    {18,17,19},
    {16,15,14},
    {18,16,17},
    {17,15,14},
    {16,15,17},
    {15,13,14},
    {18,17,14},
    {18,16,14},
    {15,12,13},
    {19,17,14},
    {19,18,14},
    {20,17,18},
    {20,17,19},
    {6,5,7},
    {16,13,14},
    {16,15,13},
    {22,18,19},
    {20,22,19},
    {17,13,14},
    {17,15,13},
    {20,22,16},
    {22,16,18},
    {22,16,19},
    {19,16,17},
    {18,16,15},
    {20,22,17},
    {22,17,18},
    {19,20,14},
    {15,12,14},
    {14,12,13},
    {16,15,12},
    {16,12,14},
    {16,12,13},
    {13,12,6},
    {13,6,7},
    {23,22,20},
    {8,7,5},
    {8,9,5},
    {12,6,7},
    {22,23,18},
    {23,18,20},
    {5,7,2},
    {9,7,8},
    {9,7,5},
    {22,23,16},
    {23,16,20},
    {23,16,18},
    {13,7,5},
    {13,6,5},
    {11,8,9},
    {6,7,8},
    {6,5,8},
    {8,7,2},
    {8,5,2},
    {5,6,2},
    {7,6,2},
    {8,6,2},
    {10,3,4},
    {12,6,5},
    {10,0,4},
    {10,0,3},
    {10,4,1},
    {10,3,1},
    {10,0,1},
    {21,11,10},
    {11,9,10},
    {10,11,3},
    {13,7,8},
    {25,24,21},
    {11,8,10},
    {10,11,4},
    {10,11,0},
    {24,23,22},
    {11,5,8},
    {9,11,5},
    {11,7,8},
    {11,7,9},
    {21,9,11},
    {21,8,11},
    {21,25,11},
    {25,24,22},
    {25,22,11},
    {25,24,11},
    {21,24,11},
    {25,22,21},
    {24,22,21},
    {25,24,23},
    {21,25,10},
    {24,23,21},
    {21,24,10}
  };
  return triangles2[line][column];
}

struct test {
    int id;
    int x, y;
};
typedef struct test Test;

int main(int argc, char *argv[]) {
  //Serial.begin(115200);

  printf("\nSize of Test ");
  printf("%d", sizeof(Test));
  printf("Size of MTriplet ");
  printf("%d", sizeof(MTriplet));
  printf("Size of Minutia ");
  printf("%d", sizeof(Minutia));
  printf("Size of Node ");
  printf("%d", sizeof(Node));

  Minutia qms[qms_size], tms[tms_size];

  initializeMinutiaeSets(qms, tms);

  Minutia* tempMinutia1 = (Minutia*) malloc(sizeof(Minutia));
  Minutia* tempMinutia2 = (Minutia*) malloc(sizeof(Minutia));
  Minutia* tempMinutia3 = (Minutia*) malloc(sizeof(Minutia));

  // Initialize m-triplets
  firstNodeOfQueryMTripletLinkedList = initialize();
  firstNodeOfTemplateMTripletLinkedList = initialize();

  int i;
  for(i=0; i<num_triangles1; i++) {
    *tempMinutia1 = qms[getTriangle1ValueAt(i, 0)];
    *tempMinutia2 = qms[getTriangle1ValueAt(i, 1)];
    *tempMinutia3 = qms[getTriangle1ValueAt(i, 2)];
    Node* newNode = createMTriplet(*tempMinutia1, *tempMinutia2, *tempMinutia3);
    firstNodeOfQueryMTripletLinkedList = insert(firstNodeOfQueryMTripletLinkedList, newNode);
  }
  for(i=0; i<num_triangles2; i++) {
    *tempMinutia1 = tms[getTriangle2ValueAt(i, 0)];
    *tempMinutia2 = tms[getTriangle2ValueAt(i, 1)];
    *tempMinutia3 = tms[getTriangle2ValueAt(i, 2)];
    Node* newNode = createMTriplet(*tempMinutia1, *tempMinutia2, *tempMinutia3);
    firstNodeOfTemplateMTripletLinkedList = insert(firstNodeOfTemplateMTripletLinkedList, newNode);
  }

  // 3 - Set euclidean distances
  Node* auxNode = (Node*) malloc(sizeof(Node));
  auxNode = firstNodeOfQueryMTripletLinkedList;
  printf("\nQuery Euclidean distances\n");
  while (auxNode->next != 0) {
    auxNode = auxNode->next;
    MTriplet* queryTriplet = (MTriplet*) auxNode->info;
    queryTriplet->d[0] = ed(queryTriplet->m[0].x, queryTriplet->m[0].y, queryTriplet->m[1].x, queryTriplet->m[1].y);
    queryTriplet->d[1] = ed(queryTriplet->m[1].x, queryTriplet->m[1].y, queryTriplet->m[2].x, queryTriplet->m[2].y);
    queryTriplet->d[2] = ed(queryTriplet->m[2].x, queryTriplet->m[2].y, queryTriplet->m[0].x, queryTriplet->m[0].y);
    /*printf("%d %d %d\n", queryTriplet->m[0].id, queryTriplet->m[1].id, queryTriplet->m[2].id);
    Serial.print(" d1:");
    Serial.print(queryTriplet->d[0]);
    Serial.print(" d2:");
    Serial.print(queryTriplet->d[1]);
    Serial.print(" d3:");
    Serial.println(queryTriplet->d[2]);*/
  }
  auxNode = firstNodeOfTemplateMTripletLinkedList;
  printf("\nTemplate Euclidean distances\n");
  while (auxNode->next != 0) {
    auxNode = auxNode->next;
    MTriplet* templateTriplet = (MTriplet*) auxNode->info;
    templateTriplet->d[0] = ed(templateTriplet->m[0].x, templateTriplet->m[0].y, templateTriplet->m[1].x, templateTriplet->m[1].y);
    templateTriplet->d[1] = ed(templateTriplet->m[1].x, templateTriplet->m[1].y, templateTriplet->m[2].x, templateTriplet->m[2].y);
    templateTriplet->d[2] = ed(templateTriplet->m[2].x, templateTriplet->m[2].y, templateTriplet->m[0].x, templateTriplet->m[0].y);
    /*printf("%d %d %d\n", templateTriplet->m[0].id, templateTriplet->m[1].id, templateTriplet->m[2].id);
    Serial.print(" d1:");
    Serial.print(templateTriplet->d[0]);
    Serial.print(" d2:");
    Serial.print(templateTriplet->d[1]);
    Serial.print(" d3:");
    Serial.println(templateTriplet->d[2]);*/
  }

  // 4 - Calculate alphas
  auxNode = firstNodeOfQueryMTripletLinkedList;
  //Serial.println("\nQuery Triplet Alphas\n");
  while (auxNode->next != 0) {
    auxNode = auxNode->next;
    MTriplet* queryTriplet = (MTriplet*) auxNode->info;
    calculateAlphas(queryTriplet);
  }
  auxNode = firstNodeOfTemplateMTripletLinkedList;
  //Serial.println("\nTemplate Triplet Alphas\n");
  while (auxNode->next != 0) {
    auxNode = auxNode->next;
    MTriplet* templateTriplet = (MTriplet*) auxNode->info;
    calculateAlphas(templateTriplet);
  }
  // 5 - Calculate betas
  //Serial.println("\nQuery Betas\n");
  auxNode = firstNodeOfQueryMTripletLinkedList;
  while (auxNode->next != 0) {
    auxNode = auxNode->next;
    MTriplet* queryTriplet = (MTriplet*) auxNode->info;
    calculateBetas(queryTriplet);
  }
  //Serial.println("\nTemplate Betas\n");
  auxNode = firstNodeOfTemplateMTripletLinkedList;
  while (auxNode->next != 0) {
    auxNode = auxNode->next;
    MTriplet* templateTriplet = (MTriplet*) auxNode->info;
    calculateBetas(templateTriplet);
    //printf("%d,%d,%d\n", templateTriplet->b[0], templateTriplet->b[1], templateTriplet->b[2]);
  }

  printf("\nPoint A\n");
  Minutia mmp[30][2];//TODO tirar tamanho fixo
  int matchingPairs = lmm(qms, tms, mmp);
  printf("Matching Pairs: ");
  printf("%d\n", matchingPairs);
  int n = gmm(mmp, matchingPairs);
  printf("Matching minutiae: ");
  printf("%d\n", n);
  float similarity = ssc(n);
  printf("Similarity: ");
  printf("%f\n", similarity);
  printf("Finished");
  return 0;
}
