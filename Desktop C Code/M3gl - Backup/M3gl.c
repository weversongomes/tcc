/*
 * Created by: Weverson dos Santos Gomes (weversondsg@gmail.com)
 * Comments by: Weverson dos Santos Gomes (weversondsg@gmail.com)
 * Description: C version of the M3gl algorithm based on the work "Improving Fingerprint
 *              Verification Using Minutiae Triplets", by Miguel Angel Medina Pérez
 *              The full documentation is available in
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "linkedlist.h"
#include "mtriplet.h"
#include "minutia.h"
// ----------------------------------------
//               Defines
// ----------------------------------------
#define tl (12)
#define tg (12)
#define ta (30)
#define c (4)
#define MTYPE (3)
// ----------------------------------------
//           Global Variables
// ----------------------------------------
Node* firstNodeOfQueryMTripletLinkedList;
Node* firstNodeOfTemplateMTripletLinkedList;
// ----------------------------------------
//              Functions
// ----------------------------------------
/**
 * Euclidean distance between two points
 */
float ed(int x1, int y1, int x2, int y2)
{
    float difx = x1 - x2;
    float dify = y1 - y2;
    return sqrt(difx * difx + dify * dify);
}

/**
 * Angular distance between two angles
 */
int adpi(int a1, int a2)
{
    int ad1, ad2;               // angular distances
    ad1 = fabs(a1 - a2);
    ad2 = 360 - fabs(a1 - a2);
    if (ad1 < ad2)
        return ad1;
    else return ad2;
}

/**
 * Angular distance between two angles in clockwise sense
 */
int ad2pi(int a1, int a2)
{
    if (a2 >= a1)
        return a2 - a1;
    else return a2 - a1 + 360;
}

/**
 * Sort in ascending order
 */
void sortAscending(float *a, int n)
{
	float t;

	for (int j=0 ; j<(n-1) ; j++) {
		for (int i=0 ; i<(n-1) ; i++) {
			if (a[i+1] < a[i]) {
				t = a[i];
				a[i] = a[i + 1];
				a[i + 1] = t;
			}
		}
	}
}

/**
 * Converts from radians to degree
 */
float to_degrees(float radians) {
    return radians * (180.0 / M_PI);
}

/**
 * Two m-triplets are dissimilar if their minutiae directions differ more than π/4
 */
int so(MTriplet m1, MTriplet m2)
{
    if (adpi(m1.m[0].o, m2.m[0].o) > 45 || adpi(m1.m[1].o, m2.m[1].o) > 45 || adpi(m1.m[2].o, m2.m[2].o) > 45)
        return 0;

    return 1;
}

/**
 * Compares m-triplets in terms of the side lengths of the triangle formed by the triplet minutiae
 */
float sd(MTriplet m1, MTriplet m2)
{
    if (fabs(m1.d[0] - m2.d[0]) > tl || fabs(m1.d[1] - m2.d[1]) > tl || fabs(m1.d[2] - m2.d[2]) > tl)
        return 0;

    float max = 0;
    float aux;
    for (int i = 0; i < 3; i++)
    {
        aux = fabs(m1.d[i] - m2.d[i]);
        if (max < aux)
            max = aux;
    }
    //printf("max = %f\n", max);
    return 1 - max / tl;
}

/**
 * Compares m-triplets based on the angles formed by minutiae directions and the sides of the triangles (angles α)
 */
float sa(struct mTriplet m1, struct mTriplet m2)
{
    float max = 0;
    for (int i = 0; i < 6; i++)
    {
        if (adpi(m1.a[i], m2.a[i]) > ta)
        {
            //printf("Diferença alpha = %d\n", adpi(m1.a[i], m2.a[i]));
            return 0;
        }
        int angle = adpi(m1.a[i], m2.a[i]);
        if (angle > max)
        {
            max = angle;
        }
    }
    float result = 1 - max / ta;
    //printf ("Valor de sa %f\n", result);
    return result;
}

/**
 * Compares m-triplets based on relative minutiae directions (angles β)
 */
float sb(struct mTriplet m1, struct mTriplet m2)
{
    int max = 0;
    for (int i = 0; i < 3; i++)
    {
        int angle = adpi(m1.b[i], m2.b[i]);
        if (angle > ta)
        {
            return 0;
        }
        if (angle > max)
        {
            max = angle;
        }
    }
    return 1 - (float)max / (float)ta;
}

float computeAngle(float dX, float dY) {
    if (dX > 0 && dY >= 0)
        return atan(dY / dX);
    if (dX > 0 && dY < 0)
        return atan(dY / dX) + 2 * M_PI;
    if (dX < 0)
        return atan(dY / dX) + M_PI;
    if (dX == 0 && dY > 0)
        return M_PI / 2;
    if (dX == 0 && dY < 0)
        return 3 * M_PI / 2;
}

/**
 * Calculates the alpha values for a given minutia
 */
calculateAlphas(MTriplet* mTriplet)
{
    float dx, dy;
    int counter = 0;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (i != j) {
                float dx = mTriplet->m[i].x - mTriplet->m[j].x;
                float dy = mTriplet->m[i].y - mTriplet->m[j].y;
                float lineAngle = computeAngle(dx, dy) * 57.2958;
                mTriplet->a[counter] = ad2pi(mTriplet->m[i].o, lineAngle);
                counter++;
            }
        }
    }
    //printf("(%d,%d,%d) %d | %d | %d | %d | %d | %d\n", mTriplet->m[0].id, mTriplet->m[1].id, mTriplet->m[2].id, mTriplet->a[0], mTriplet->a[1], mTriplet->a[2], mTriplet->a[3], mTriplet->a[4], mTriplet->a[5]);
}

/**
 * Calculates the beta values for a given minutia
 */
calculateBetas(MTriplet* m1)
{
    m1->b[0] = ad2pi(m1->m[0].o, m1->m[1].o);
    m1->b[1] = ad2pi(m1->m[1].o, m1->m[2].o);
    m1->b[2] = ad2pi(m1->m[2].o, m1->m[0].o);
    //printf("%d | %d | %d\n", m1->b[0], m1->b[1], m1->b[2]);
}

/**
 * Shifts an m-Triplet in clockwise sense
 */
MTriplet shift(MTriplet m1)
{
    //printf("before rotation: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    struct minutia m = m1.m[0];
    m1.m[0] = m1.m[1];
    m1.m[1] = m1.m[2];
    m1.m[2] = m;
    //printf("after rotation: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    return m1;
}

/**
 * Base similarity function
 */
float sPart(MTriplet m1, MTriplet m2)
{
    if (so(m1, m2) == 0 || sd(m1, m2) == 0 || sa(m1, m2) == 0 || sb(m1, m2) == 0) {
        return 0;
    }

    //printf("____________________________________________________\n");
    //printf("m1: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    //printf("m2: %d | %d | %d\n", m2.m[0].id, m2.m[1].id, m2.m[2].id);
    //printf("m1 d: %f | %f | %f\n", m1.d[0], m1.d[1], m1.d[2]);
    //printf("sPart: %d | %f | %f | %f\n", so(m1, m2), sd(m1, m2), sa(m1, m2), sb(m1, m2));
    //printf("Diffs sd: %f | %f | %f\n", fabs(m1.d[0] - m2.d[0]), fabs(m1.d[1] - m2.d[1]), fabs(m1.d[2] - m2.d[2]));
    return 1 - (1 - sd(m1, m2)) * (1 - sa(m1, m2)) * (1 - sb(m1, m2));
}

/**
 * M-triplets similarity
 * Accurately distinguish between similar and non-similar m-triplets
 */
float sInv(MTriplet m1, MTriplet m2, MTriplet* newTriplet)
{
	float m1distances[3];
	float m2distances[3];

	m1distances[0] = m1.d[0];
	m1distances[1] = m1.d[1];
	m1distances[2] = m1.d[2];
	m2distances[0] = m2.d[0];
	m2distances[1] = m2.d[1];
	m2distances[2] = m2.d[2];

	sortAscending(m1distances, 3);
	sortAscending(m2distances, 3);

	if ((fabs(m1distances[0] - m2distances[0]) > tl) || (fabs(m1distances[1] - m2distances[1]) > tl) || (fabs(m1distances[2] - m2distances[2]) > tl)) {
		return 0;
	}

    float max = 0;
    float aux;
    MTriplet newm2;
    max = sPart(m1, m2);
    newm2 = shift(m2);

    rebuildTriplet(&newm2);

    aux = sPart(m1, newm2);
    if (aux > max)
    {
        max = aux;
        m2 = newm2;
        *newTriplet = newm2;
    }
    newm2 = shift(newm2);

    rebuildTriplet(&newm2);

    aux = sPart(m1, newm2);
    if (aux > max)
    {
        max = aux;
        m2 = newm2;
        *newTriplet = newm2;
    }

    return max;
}

void rebuildTriplet(MTriplet* mTriplet) {
    mTriplet->d[0] = ed(mTriplet->m[0].x, mTriplet->m[0].y, mTriplet->m[1].x, mTriplet->m[1].y);
    mTriplet->d[1] = ed(mTriplet->m[1].x, mTriplet->m[1].y, mTriplet->m[2].x, mTriplet->m[2].y);
    mTriplet->d[2] = ed(mTriplet->m[2].x, mTriplet->m[2].y, mTriplet->m[0].x, mTriplet->m[0].y);
    calculateAlphas(mTriplet);
    calculateBetas(mTriplet);
}
// ----------------------------------------
//            M3gl Algorithm
// ----------------------------------------
/**
 * Local Minutiae Matching
 *   This step finds the similar m-triplets in the template fingerprint using binary search. Then, it
 *   sorts all matching pairs according to the similarity value and finds the local matching minutiae.
 * qms - query fingerprint minutiae set
 * tms - template fingerprint minutiae set
 * qts - query fingerprint m-triplet set
 * tts - template fingerprint m-triplet set
 * mmp - local matching minutiae pairs
 * returns the size of the array containing all local matching minutiae pairs
 */
int lmm(Minutia qms[], Minutia tms[], Minutia mmp[][2])
{
	MTriplet localMatchingMTripletsPairs[1000][2]; 	//todo tirar valor fixo
	float similarities[1000];						//todo tirar valor fixo
	int mmtpSize = 0;
	Node* auxNodeQ = (Node*) malloc(sizeof(Node));
	Node* auxNodeT = (Node*) malloc(sizeof(Node));
	auxNodeQ = firstNodeOfQueryMTripletLinkedList;
	// For each query m-triplet perform binary search looking for the template m-triplets with similarity value higher than 0
	while (auxNodeQ->next != NULL) {
		auxNodeQ = auxNodeQ->next;
		MTriplet* queryTriplet = auxNodeQ->info;
		auxNodeT = firstNodeOfTemplateMTripletLinkedList;
		while (auxNodeT->next != NULL) {
			auxNodeT = auxNodeT->next;
			MTriplet* templateTriplet = auxNodeT->info;

            float similarity = sInv(*queryTriplet, *templateTriplet, templateTriplet);
            //printf("Similarity - %f\n", similarity);
            if (similarity > 0)
            {
                localMatchingMTripletsPairs[mmtpSize][0] = *queryTriplet;
                localMatchingMTripletsPairs[mmtpSize][1] = *templateTriplet;
                similarities[mmtpSize] = similarity;

            	mmtpSize++;
            }
        }
    }
    // Sort in descendant order all matching pairs (qts, tts) in mmtp according to the similarity value

    printf("mmtpSize = %d\n", mmtpSize);
    int tmp;
    MTriplet tmpmmtp[2];
    for (int i = 0; i < mmtpSize; i++)
    {
        for (int j = 0; j < mmtpSize-i; j++)
        {
            if (similarities[j] < similarities[j+1])
            {
                tmp = similarities[j+1];
                similarities[j+1] = similarities[j];
                similarities[j] = tmp;
                tmpmmtp[0] = localMatchingMTripletsPairs[j+1][0];
                tmpmmtp[1] = localMatchingMTripletsPairs[j+1][1];
                localMatchingMTripletsPairs[j+1][0] = localMatchingMTripletsPairs[j][0];
                localMatchingMTripletsPairs[j+1][1] = localMatchingMTripletsPairs[j][1];
                localMatchingMTripletsPairs[j][0] = tmpmmtp[0];
                localMatchingMTripletsPairs[j][1] = tmpmmtp[1];
            }
        }
    }
    printf("mmtpSize = %d\n", mmtpSize);
    // print all matching pairs
    printf("\nMatching triplet pairs\n");
    for (int j = 0; j < mmtpSize; j++)
    {
    	printf("(%d,%d,%d)(%d,%d,%d)\n", localMatchingMTripletsPairs[j][0].m[0].id, localMatchingMTripletsPairs[j][0].m[1].id, localMatchingMTripletsPairs[j][0].m[2].id, localMatchingMTripletsPairs[j][1].m[0].id, localMatchingMTripletsPairs[j][1].m[1].id, localMatchingMTripletsPairs[j][1].m[2].id);
    }
    MTriplet mt0, mt1;
    int mmpcounter = 0;

    printf("\nMinutiae pairs\n");
    for (int i = 0; i < mmtpSize; i++)
    {
        int containsMinutia1 = 0;
        int containsMinutia2 = 0;
        mt0 = localMatchingMTripletsPairs[i][0];
        mt1 = localMatchingMTripletsPairs[i][1];
        for (int j = 0; j < 3; j++)
        {
        	containsMinutia1 = 0;
        	containsMinutia2 = 0;
            for (int k = 0; k < mmpcounter; k++)
            {
                if (mt0.m[j].x == mmp[k][0].x && mt0.m[j].y == mmp[k][0].y)
                {
                    containsMinutia1 = 1;
                }
                if (mt1.m[j].x == mmp[k][1].x && mt1.m[j].y == mmp[k][1].y)
                {
                    containsMinutia2 = 1;
                }
            }
            if (!containsMinutia1 || !containsMinutia2)
            {
                printf("(%d,%d)(%d,%d)\n", mt0.m[j].x, mt0.m[j].y, mt1.m[j].x, mt1.m[j].y);
                mmp[mmpcounter][0] = mt0.m[j];
                mmp[mmpcounter][1] = mt1.m[j];
                mmpcounter++;
            }
        }
    }

    return mmpcounter;
}

/**
 * Global Minutiae Matching
 * This step uses every minutiae pair as a reference pair for fingerprint rotation and performs a query
 * minutiae transformation for each reference pair. Later, it selects the transformation that maximizes
 * the amount of matching minutiae.
 * Returns the maximum number of matching minutiae computed after performing all fingerprint rotations.
 */
int gmm(Minutia mmp[][2], int mmpsize)
{
    int n = 0;      // maximum number of matching minutiae
    Minutia bestE[mmpsize][2];
    printf("\nGlobal Minutiae Matching\n");
    for (int i = 0; i < mmpsize; i++)
    {
        printf("RefPair (%d,%d)(%d,%d)\n", mmp[i][0].x, mmp[i][0].y, mmp[i][1].x, mmp[i][1].y);
        Minutia E[mmpsize][2];
        int sizeE = 0;

        for (int j = 0; j < mmpsize; j++)
        {
            int contains = 0;
            for (int k = 0; k <= sizeE; k++)
            {
                if (((mmp[j][0].x == E[k][0].x) && (mmp[j][0].y == E[k][0].y)) || ((mmp[j][1].x == E[k][1].x) && (mmp[j][1].y == E[k][1].y)))
                {
                    contains = 1;
                }
            }
            if (contains == 0)
            {
                Minutia ql;
                int dAngle = mmp[i][1].o - mmp[i][0].o;
                int newAngle = mmp[j][0].o + dAngle;
                float seno = sin(dAngle / 57.2958);
                float coss = cos(dAngle / 57.2958);
                printf("    Cosseno: %f", coss);
                ql.o = (newAngle > 360) ? newAngle - 360 : (newAngle < 0) ? newAngle + 360 : newAngle;
                ql.x = (mmp[j][0].x - mmp[i][0].x) * coss - (mmp[j][0].y - mmp[i][0].y) * seno + mmp[i][1].x;
                ql.y = (mmp[j][0].x - mmp[i][0].x) * seno + (mmp[j][0].y - mmp[i][0].y) * coss + mmp[i][1].y;
                //ql.x = cos(dAngle) * (mmp[j][0].x - mmp[i][0].x) - sin(dAngle) * (mmp[j][0].y - mmp[i][0].y) + mmp[i][1].x;
                //ql.y = sin(dAngle) * (mmp[j][0].x - mmp[i][0].x) + cos(dAngle) * (mmp[j][0].y - mmp[i][0].y) + mmp[i][1].y;
                printf("    Pair (%d,%d)(%d,%d)\n", mmp[j][0].x, mmp[j][0].y, mmp[j][1].x, mmp[j][1].y);
                //printf("    mAngle %f\n", mmp[j][0].o);
                //printf("    dAngle %d\n", dAngle);
                printf("    newAngle %d\n", newAngle);
                printf("    Mapped minutia: (%d,%d)>%f\n", ql.x, ql.y, ql.o);
                printf("---------------------\n");
                if (fabs(ed(ql.x, ql.y, mmp[j][1].x, mmp[j][1].y)) <= tg && adpi(ad2pi(mmp[i][1].o, mmp[i][0].o), ad2pi(mmp[j][1].o, mmp[j][0].o)) <= ta && adpi(ql.o, mmp[j][1].o) <= ta)
                {
                    E[sizeE][0] = mmp[j][0];
                    E[sizeE][1] = mmp[j][1];
                    sizeE++;
                }
            }
        }
        if (n < sizeE)
        {
            n = sizeE;
            for (int w = 0; w < n; w++)
            {
            	bestE[w][0] = E[w][0];
            	bestE[w][1] = E[w][1];
            }

        }
    }
    printf("FINAL RESULT\n");
    for (int w = 0; w < n; w++)
    {
    	printf("(%d,%d)(%d,%d)\n", bestE[w][0].x, bestE[w][0].y, bestE[w][1].x, bestE[w][1].y);
    }
    return n;
}

/**
 *  Similarity Score Computation
 */
float ssc(float qms_size, float tms_size, float n)
{
    return (n*n)/(qms_size * tms_size);
}

/**
 *  Initialize triangles with values from file
 */
Node* initializeTriangles(Minutia minutiaeSet[], char trianglesFile[], Node* firstNode)
{
	int cvar;
	FILE *file;
	file = fopen(trianglesFile, "r");
	int value = 0, line = 0;
	int aux = 0;

    Minutia tempMinutiaArray[3];

	if (file) {
		fscanf (file, "%d", &value);
	    //printf("PRITING TRIANGLES\n");
		while ((cvar = getc(file)) != EOF) {
			if (aux == 0) {
				//tts[line].m[0] = qts[line].m[0] = qms[cvar - '0']; // TODO parse int]
				aux++;

				tempMinutiaArray[0] = minutiaeSet[value];
	    	    //printf("[%d", tempMinutiaArray[0].id);
			} else if (aux == 1) {
				aux++;

				tempMinutiaArray[1] = minutiaeSet[value];
	    	    //printf(",%d", tempMinutiaArray[1].id);
			} else if (aux == 2) {
				aux = 0;
				line++;

				tempMinutiaArray[2] = minutiaeSet[value];
	    	    //printf(",%d]\n", tempMinutiaArray[2].id);
				Node* newNode = createMTriplet(tempMinutiaArray[0], tempMinutiaArray[1], tempMinutiaArray[2]);
				firstNode = insert(firstNode, newNode);
			}
			fscanf (file, "%d", &value);
		}
		fclose(file);
	}
	return firstNode;
}

int getFileSize(char filename[])
{
	int cvar;
	FILE *file;
	file = fopen(filename, "r");
	int i = 0, lines = 0;
	if (file) {
		fscanf (file, "%d", &i);
	    while ((cvar = getc(file)) != EOF) {
	    	if (cvar == '\n' || cvar == '\r') {
	    		lines++;
	    	}
	    	fscanf (file, "%d", &i);
	    }
	    fclose(file);
	}
	printf("Linhas encontradas - %d\n", lines);
	return lines;
}
// ----------------------------------------
//                 Main
// ----------------------------------------
int main(void)
{
	int qms_size = 0, tms_size = 0;
	char minutiae1[] = "Input/Extractor para M3gl no Framework/minutiae/103_1.txt";
	char minutiae2[] = "Input/Extractor para M3gl no Framework/minutiae/103_4.txt";
	char triangles1[] = "Input/Extractor para M3gl no Framework/mtriplets/103_1.txt";
	char triangles2[] = "Input/Extractor para M3gl no Framework/mtriplets/103_4.txt";

	qms_size = getFileSize(minutiae1);
	//printf("qms_size - %d\n", qms_size);

	tms_size = getFileSize(minutiae2);
	//printf("tms_size - %d\n", tms_size);

    Minutia qms[qms_size], tms[tms_size];
    // 1 - Initialize minutiae
	int counter = 0, setcounter = 0;
	FILE *file;
	float i = 0;
	int aux = 0;
	file = fopen(minutiae1, "r");
	int cvar;
	if (file) {
        //printf("Printing query minutiae\n");
		fscanf (file, "%f", &i);
	    while ((cvar = getc(file)) != EOF) {
	    	if (aux == 0) {
                qms[counter].id = counter;
	    	    qms[counter].x = i;
	    	    //printf("(%d,", qms[counter].x);
		    	aux++;
	    	} else if (aux == 1) {
	    	    qms[counter].y = i;
	    	    //printf("%d)\n", qms[counter].y);
		    	aux++;
	    	} else {
	    	    setcounter = 1;
	    	    qms[counter].o = i * 57.2958;
		    	aux = 0;
		    	if (setcounter == 1) {
			    	setcounter = 0;
			    	counter++;
		    	}
	    	}
	    	fscanf (file, "%f", &i);
	    }
	}
    fclose(file);

	counter = 0, setcounter = 0;
	i = 0;
	aux = 0;
	file = fopen(minutiae2, "r");
	if (file) {
		fscanf (file, "%f", &i);
	    while ((cvar = getc(file)) != EOF) {
	    	if (aux == 0) {
                tms[counter].id = counter;
	    	    tms[counter].x = i;
		    	aux++;
	    	} else if (aux == 1) {
	    	    tms[counter].y = i;
		    	aux++;
	    	} else {
	    	    setcounter = 1;
	    	    tms[counter].o = i * 57.2958;
		    	aux = 0;
		    	if (setcounter == 1) {
			    	setcounter = 0;
			    	counter++;
		    	}
	    	}
	    	fscanf (file, "%f", &i);
	    }
	}
    fclose(file);

    // 2 - Initialize triangles
    firstNodeOfQueryMTripletLinkedList = initialize();
    firstNodeOfTemplateMTripletLinkedList = initialize();
    firstNodeOfQueryMTripletLinkedList = initializeTriangles(qms, triangles1, firstNodeOfQueryMTripletLinkedList);
    firstNodeOfTemplateMTripletLinkedList = initializeTriangles(tms, triangles2, firstNodeOfTemplateMTripletLinkedList);

    // 3 - Set euclidean distances
    Node* auxNode = (Node*) malloc(sizeof(Node));
    auxNode = firstNodeOfQueryMTripletLinkedList;

	printf("Query Euclidean distances\n");
	while (auxNode->next != NULL) {
		auxNode = auxNode->next;
		MTriplet* queryTriplet = auxNode->info;
		//printf("Valor x de m1 da triplet - %d\n", queryTriplet->m[0].x);

		queryTriplet->d[0] = ed(queryTriplet->m[0].x, queryTriplet->m[0].y, queryTriplet->m[1].x, queryTriplet->m[1].y);
		queryTriplet->d[1] = ed(queryTriplet->m[1].x, queryTriplet->m[1].y, queryTriplet->m[2].x, queryTriplet->m[2].y);
		queryTriplet->d[2] = ed(queryTriplet->m[2].x, queryTriplet->m[2].y, queryTriplet->m[0].x, queryTriplet->m[0].y);
		printf("%d %d %d d1:%f d2:%f d3:%f\n", queryTriplet->m[0].id, queryTriplet->m[1].id, queryTriplet->m[2].id, queryTriplet->d[0], queryTriplet->d[1], queryTriplet->d[2]);
	}

	auxNode = firstNodeOfTemplateMTripletLinkedList;
	printf("Template Euclidean distances\n");
	while (auxNode->next != NULL) {
		auxNode = auxNode->next;
		MTriplet* templateTriplet = auxNode->info;

		templateTriplet->d[0] = ed(templateTriplet->m[0].x, templateTriplet->m[0].y, templateTriplet->m[1].x, templateTriplet->m[1].y);
		templateTriplet->d[1] = ed(templateTriplet->m[1].x, templateTriplet->m[1].y, templateTriplet->m[2].x, templateTriplet->m[2].y);
		templateTriplet->d[2] = ed(templateTriplet->m[2].x, templateTriplet->m[2].y, templateTriplet->m[0].x, templateTriplet->m[0].y);
        printf("%d %d %d d1:%f d2:%f d3:%f\n", templateTriplet->m[0].id, templateTriplet->m[1].id, templateTriplet->m[2].id, templateTriplet->d[0], templateTriplet->d[1], templateTriplet->d[2]);
	}

	// 4 - Calculate alphas
	auxNode = firstNodeOfQueryMTripletLinkedList;
    printf("Query Triplet Alphas\n");
	while (auxNode->next != NULL) {
		auxNode = auxNode->next;
		MTriplet* queryTriplet = auxNode->info;
		calculateAlphas(queryTriplet);
	}
	auxNode = firstNodeOfTemplateMTripletLinkedList;
	printf("\nTemplate Triplet Alphas\n");
	while (auxNode->next != NULL) {
		auxNode = auxNode->next;
		MTriplet* templateTriplet = auxNode->info;
		calculateAlphas(templateTriplet);
	}

	// 5 - Calculate betas
    printf("\nQuery Betas\n");
	auxNode = firstNodeOfQueryMTripletLinkedList;
	while (auxNode->next != NULL) {
		auxNode = auxNode->next;
		MTriplet* queryTriplet = auxNode->info;
		calculateBetas(queryTriplet);
	}
    printf("\nTemplate Betas\n");
	auxNode = firstNodeOfTemplateMTripletLinkedList;
	while (auxNode->next != NULL) {
		auxNode = auxNode->next;
		MTriplet* templateTriplet = auxNode->info;
		calculateBetas(templateTriplet);
		printf("%d,%d,%d\n", templateTriplet->b[0], templateTriplet->b[1], templateTriplet->b[2]);
	}

    Minutia mmp[1000][2];//TODO tirar tamanho fixo
    int matchingPairs = lmm(qms, tms, mmp);
    printf("Matching Pairs = %d\n", matchingPairs);
    int n = gmm(mmp, matchingPairs);
    //printf("Matching minutiae = %d\n", n);
    float similarity = ssc(qms_size, tms_size, n);
    printf("\nSimilarity: %f. Matching minutiae: %d.\n", similarity, n);

    free(auxNode);

    return 0;
}
