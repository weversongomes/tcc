#ifndef _linkedlist_h
#define _linkedlist_h

#include "minutia.h"

/* Generic list */
struct node {
   void *info;
   struct node *next;
};
typedef struct node Node;

Node* initialize();

Node* createMTriplet(Minutia m0, Minutia m1, Minutia m2);

Node* insert(Node* firstNode, Node* newNode);

void imprime(Node* l);

int isEmpty(Node* l);

Node* search(Node* l, int v);

#endif
