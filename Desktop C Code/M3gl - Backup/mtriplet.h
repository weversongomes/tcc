#ifndef _mtriplet_h
#define _mtriplet_h

#include "minutia.h"

struct mTriplet
{
    Minutia m[3];               // minutiae
    float d[3];                 // the Euclidean distance between the minutiae different than pi
    int a[6];               	// angle required to rotate the direction θ of a minutia to superpose it to the vectors associated with the other two minutiae in the triplet
    int b[3];                   // angle required to rotate the direction of the minutia pk in order to superpose it to the direction of the minutia pj
};
typedef struct mTriplet MTriplet;

#endif
