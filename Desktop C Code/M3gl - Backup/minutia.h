#ifndef _minutia_h
#define _minutia_h

typedef struct minutia
{
    int id;
    int x, y;                   // position
    float o;                      // minutia direction
};
typedef struct minutia Minutia;

#endif
