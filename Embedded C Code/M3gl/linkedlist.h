#ifndef _linkedlist_h
#define _linkedlist_h

#include "linkedlist.c"

Node* initialize ();

Node* createMTriplet (Minutia m0, Minutia m1, Minutia m2);

Node* insert (Node* firstNode, Node* newNode);

int isEmpty (Node* l);

#endif
