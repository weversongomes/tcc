
#include "minutia.h"
#include "mTriplet.h"

/* Generic list */
struct node {
   void *info;
   struct node *next;
};
typedef struct node Node;

Node* initialize ()
{
  return 0;
}

Node* createMTriplet (Minutia m0, Minutia m1, Minutia m2)
{
  MTriplet* mTriplet;
  Node* node;

  mTriplet = (MTriplet*) malloc(sizeof(MTriplet));
  mTriplet->m[0] = m0;
  mTriplet->m[1] = m1;
  mTriplet->m[2] = m2;

  node = (Node*) malloc(sizeof(Node));
  node->info = mTriplet;
  node->next = 0;
  return node;
}

Node* insert (Node* firstNode, Node* newNode)
{
  if (firstNode != 0) {
    newNode->next = firstNode->next;
    firstNode->next = newNode;
    return firstNode;
  }
  firstNode = (Node*) malloc(sizeof(Node));
  newNode->next = 0;
  firstNode->next = newNode;
  return firstNode;
}

int isEmpty (Node* l)
{
  if (l == 0)
    return 1;
  else
    return 0;
}
