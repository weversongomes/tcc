
typedef struct minutia
{
    int id;
    int x, y;                   /* position */
    float o;                    /* minutia direction */
};
typedef struct minutia Minutia;
