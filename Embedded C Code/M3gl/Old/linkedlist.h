#ifndef _linkedlist_h
#define _linkedlist_h

#include "minutia.h"
#include "mTriplet.h"

/* Generic list */
struct node {
   void *info;
   struct node *next;
};
typedef struct node Node;

Node* initialize ()
{
	return NULL;
}

Node* createMTriplet (Minutia m0, Minutia m1, Minutia m2)
{
	MTriplet* mTriplet;
	Node* node;
	// aloca MTriplet
	mTriplet = (MTriplet*) malloc(sizeof(MTriplet));
	mTriplet->m[0] = m0;
	mTriplet->m[1] = m1;
	mTriplet->m[2] = m2;
	// aloca n�
	node = (Node*) malloc(sizeof(Node));
	node->info = mTriplet;
	node->next = NULL;
	return node;
}

Node* insert (Node* firstNode, Node* newNode)
{
	if (firstNode != NULL) {
		newNode->next = firstNode->next;
	    //printf("new node.next.next %p \n", newNode->next->next);
		firstNode->next = newNode;
		return firstNode;
	}
	firstNode = (Node*) malloc(sizeof(Node));
	newNode->next = NULL;
    printf("newNode->next %p \n", newNode->next);
	firstNode->next = newNode;
    printf("firstNode->next->next %p \n", firstNode->next->next);
	return firstNode;
}

/* fun��o imprime: imprime valores dos elementos */
void imprime (Node* l)
{
	Node* p;   /* vari�vel auxiliar para percorrer a lista */
	for (p = l; p != NULL; p = p->next)
		printf("info = %d\n", p->info);
}

/* fun��o vazia: retorna 1 se vazia ou 0 se n�o vazia */
int isEmpty (Node* l)
{
	if (l == NULL)
		return 1;
	else
		return 0;
}

/*Node* search (Node* l, int v)
{
	Node* p;
	for (p=l; p!=NULL; p=p->next)
		if (p->info == v)
			return p;
	return NULL;
}*/

#endif
