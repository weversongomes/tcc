#include "linkedlist.h"
#include "mtriplet.h"
#include "minutia.h"

#define tl (12)
#define tg (12)
#define ta (30)
#define c (4)
#define qms_size (24)
#define tms_size (23)
#define num_triangles1 (90)
#define num_triangles2 (79)
// ----------------------------------------
//           Global Variables
// ----------------------------------------
Node* firstNodeOfQueryMTripletLinkedList;
Node* firstNodeOfTemplateMTripletLinkedList;
char buffer[50];
// ----------------------------------------
//         Auxiliary Functions
// ----------------------------------------
float computeAngle(float dX, float dY) {
    if (dX > 0 && dY >= 0)
        return atan(dY / dX);
    if (dX > 0 && dY < 0)
        return atan(dY / dX) + 2 * M_PI;
    if (dX < 0)
        return atan(dY / dX) + M_PI;
    if (dX == 0 && dY > 0)
        return M_PI / 2;
    else //if (dX == 0 && dY < 0)
        return 3 * M_PI / 2;
}
/* Angular distance between two angles */
int adpi(int a1, int a2)
{
    int ad1, ad2;               // angular distances
    ad1 = fabs(a1 - a2);
    ad2 = 360 - fabs(a1 - a2);
    if (ad1 < ad2)
        return ad1;
    else return ad2;
}
/* Angular distance between two angles in clockwise sense */
int ad2pi(int a1, int a2)
{
    if (a2 >= a1)
        return a2 - a1;
    else return a2 - a1 + 360;
}

/**
 * Sort in ascending order
 */
void sortAscending(float *a, int n)
{
 float t;

  for (int j=0 ; j<(n-1) ; j++) {
    for (int i=0 ; i<(n-1) ; i++) {
      if (a[i+1] < a[i]) {
        t = a[i];
        a[i] = a[i + 1];
        a[i + 1] = t;
      }
    }
  }
}

/**
 * Converts from radians to degree
 */
float to_degrees(float radians) {
    return radians * (180.0 / M_PI);
}

/**
 * Two m-triplets are dissimilar if their minutiae directions differ more than π/4
 */
int so(MTriplet m1, MTriplet m2)
{
    if (adpi(m1.m[0].o, m2.m[0].o) > 45 || adpi(m1.m[1].o, m2.m[1].o) > 45 || adpi(m1.m[2].o, m2.m[2].o) > 45)
        return 0;

    return 1;
}

/**
 * Compares m-triplets in terms of the side lengths of the triangle formed by the triplet minutiae
 */
float sd(MTriplet m1, MTriplet m2)
{
    if (fabs(m1.d[0] - m2.d[0]) > tl || fabs(m1.d[1] - m2.d[1]) > tl || fabs(m1.d[2] - m2.d[2]) > tl)
        return 0;

    float max = 0;
    float aux;
    for (int i = 0; i < 3; i++)
    {
        aux = fabs(m1.d[i] - m2.d[i]);
        if (max < aux)
            max = aux;
    }
    //printf("max = %f\n", max);
    return 1 - max / tl;
}

/**
 * Compares m-triplets based on the angles formed by minutiae directions and the sides of the triangles (angles α)
 */
float sa(struct mTriplet m1, struct mTriplet m2)
{
    float max = 0;
    for (int i = 0; i < 6; i++)
    {
        if (adpi(m1.a[i], m2.a[i]) > ta)
        {
            //printf("Diferença alpha = %d\n", adpi(m1.a[i], m2.a[i]));
            return 0;
        }
        int angle = adpi(m1.a[i], m2.a[i]);
        if (angle > max)
        {
            max = angle;
        }
    }
    float result = 1 - max / ta;
    //printf ("Valor de sa %f\n", result);
    return result;
}

/**
 * Compares m-triplets based on relative minutiae directions (angles β)
 */
float sb(struct mTriplet m1, struct mTriplet m2)
{
    int max = 0;
    for (int i = 0; i < 3; i++)
    {
        int angle = adpi(m1.b[i], m2.b[i]);
        if (angle > ta)
        {
            return 0;
        }
        if (angle > max)
        {
            max = angle;
        }
    }
    return 1 - (float)max / (float)ta;
}
/* Euclidean distance between two points */
float ed(int x1, int y1, int x2, int y2)
{
    float difx = x1 - x2;
    float dify = y1 - y2;
    return sqrt(difx * difx + dify * dify);
}
/* Calculates the alpha values for a given minutia */
void calculateAlphas(MTriplet* mTriplet)
{
    int counter = 0;
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            if (i != j) {
                float dx = mTriplet->m[i].x - mTriplet->m[j].x;
                float dy = mTriplet->m[i].y - mTriplet->m[j].y;
                float lineAngle = computeAngle(dx, dy) * 57.2958;
                mTriplet->a[counter] = ad2pi(mTriplet->m[i].o, lineAngle);
                counter++;
            }
        }
    }
    //printf("(%d,%d,%d) %d | %d | %d | %d | %d | %d\n", mTriplet->m[0].id, mTriplet->m[1].id, mTriplet->m[2].id, mTriplet->a[0], mTriplet->a[1], mTriplet->a[2], mTriplet->a[3], mTriplet->a[4], mTriplet->a[5]);
}
/* Calculates the beta values for a given minutia */
void calculateBetas(MTriplet* m1)
{
    m1->b[0] = ad2pi(m1->m[0].o, m1->m[1].o);
    m1->b[1] = ad2pi(m1->m[1].o, m1->m[2].o);
    m1->b[2] = ad2pi(m1->m[2].o, m1->m[0].o);
}

/**
 * Shifts an m-Triplet in clockwise sense
 */
MTriplet shift(MTriplet m1)
{
    //printf("before rotation: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    struct minutia m = m1.m[0];
    m1.m[0] = m1.m[1];
    m1.m[1] = m1.m[2];
    m1.m[2] = m;
    //printf("after rotation: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    return m1;
}

/**
 * Base similarity function
 */
float sPart(MTriplet m1, MTriplet m2)
{
    if (so(m1, m2) == 0 || sd(m1, m2) == 0 || sa(m1, m2) == 0 || sb(m1, m2) == 0) {
        return 0;
    }

    //printf("____________________________________________________\n");
    //printf("m1: %d | %d | %d\n", m1.m[0].id, m1.m[1].id, m1.m[2].id);
    //printf("m2: %d | %d | %d\n", m2.m[0].id, m2.m[1].id, m2.m[2].id);
    //printf("m1 d: %f | %f | %f\n", m1.d[0], m1.d[1], m1.d[2]);
    //printf("sPart: %d | %f | %f | %f\n", so(m1, m2), sd(m1, m2), sa(m1, m2), sb(m1, m2));
    //printf("Diffs sd: %f | %f | %f\n", fabs(m1.d[0] - m2.d[0]), fabs(m1.d[1] - m2.d[1]), fabs(m1.d[2] - m2.d[2]));
    return 1 - (1 - sd(m1, m2)) * (1 - sa(m1, m2)) * (1 - sb(m1, m2));
}

void rebuildTriplet(MTriplet* mTriplet) {
    mTriplet->d[0] = ed(mTriplet->m[0].x, mTriplet->m[0].y, mTriplet->m[1].x, mTriplet->m[1].y);
    mTriplet->d[1] = ed(mTriplet->m[1].x, mTriplet->m[1].y, mTriplet->m[2].x, mTriplet->m[2].y);
    mTriplet->d[2] = ed(mTriplet->m[2].x, mTriplet->m[2].y, mTriplet->m[0].x, mTriplet->m[0].y);
    calculateAlphas(mTriplet);
    calculateBetas(mTriplet);
}

/**
 * M-triplets similarity
 * Accurately distinguish between similar and non-similar m-triplets
 */
float sInv(MTriplet m1, MTriplet m2, MTriplet* newTriplet)
{
  float m1distances[3];
  float m2distances[3];

  m1distances[0] = m1.d[0];
  m1distances[1] = m1.d[1];
  m1distances[2] = m1.d[2];
  m2distances[0] = m2.d[0];
  m2distances[1] = m2.d[1];
  m2distances[2] = m2.d[2];

  sortAscending(m1distances, 3);
  sortAscending(m2distances, 3);

  if ((fabs(m1distances[0] - m2distances[0]) > tl) || (fabs(m1distances[1] - m2distances[1]) > tl) || (fabs(m1distances[2] - m2distances[2]) > tl)) {
    return 0;
  }

    float max = 0;
    float aux;
    MTriplet newm2;
    max = sPart(m1, m2);
    newm2 = shift(m2);

    rebuildTriplet(&newm2);

    aux = sPart(m1, newm2);
    if (aux > max)
    {
        max = aux;
        m2 = newm2;
        *newTriplet = newm2;
    }
    newm2 = shift(newm2);

    rebuildTriplet(&newm2);

    aux = sPart(m1, newm2);
    if (aux > max)
    {
        max = aux;
        m2 = newm2;
        *newTriplet = newm2;
    }

    return max;
}

int lmm(Minutia qms[], Minutia tms[], Minutia mmp[][2])
{
  MTriplet localMatchingMTripletsPairs[10][2];  //todo tirar valor fixo
  float similarities[10];                       //todo tirar valor fixo
  int mmtpSize = 0;
  Node* auxNodeQ = (Node*) malloc(sizeof(Node));
  Node* auxNodeT = (Node*) malloc(sizeof(Node));
  auxNodeQ = firstNodeOfQueryMTripletLinkedList;
  // For each query m-triplet perform binary search looking for the template m-triplets with similarity value higher than 0
  while (auxNodeQ->next != NULL) {
    auxNodeQ = auxNodeQ->next;
    MTriplet* queryTriplet = (MTriplet*) auxNodeQ->info;
    auxNodeT = firstNodeOfTemplateMTripletLinkedList;
    while (auxNodeT->next != NULL) {
      auxNodeT = auxNodeT->next;
      MTriplet* templateTriplet = (MTriplet*) auxNodeT->info;

      float similarity = sInv(*queryTriplet, *templateTriplet, templateTriplet);
      //printf("Similarity - %f\n", similarity);
      if (similarity > 0)
      {
        localMatchingMTripletsPairs[mmtpSize][0] = *queryTriplet;
        localMatchingMTripletsPairs[mmtpSize][1] = *templateTriplet;
        similarities[mmtpSize] = similarity;

        mmtpSize++;
      }
    }
  }
    // Sort in descendant order all matching pairs (qts, tts) in mmtp according to the similarity value

    printf("mmtpSize = %d\n", mmtpSize);
    int tmp;
    MTriplet tmpmmtp[2];
    for (int i = 0; i < mmtpSize; i++)
    {
        for (int j = 0; j < mmtpSize-i; j++)
        {
            if (similarities[j] < similarities[j+1])
            {
                tmp = similarities[j+1];
                similarities[j+1] = similarities[j];
                similarities[j] = tmp;
                tmpmmtp[0] = localMatchingMTripletsPairs[j+1][0];
                tmpmmtp[1] = localMatchingMTripletsPairs[j+1][1];
                localMatchingMTripletsPairs[j+1][0] = localMatchingMTripletsPairs[j][0];
                localMatchingMTripletsPairs[j+1][1] = localMatchingMTripletsPairs[j][1];
                localMatchingMTripletsPairs[j][0] = tmpmmtp[0];
                localMatchingMTripletsPairs[j][1] = tmpmmtp[1];
            }
        }
    }
    printf("mmtpSize = %d\n", mmtpSize);
    // print all matching pairs
    Serial.println("\nMatching triplet pairs");
    for (int j = 0; j < mmtpSize; j++)
    {
      printf("(%d,%d,%d)(%d,%d,%d)\n", localMatchingMTripletsPairs[j][0].m[0].id, localMatchingMTripletsPairs[j][0].m[1].id, localMatchingMTripletsPairs[j][0].m[2].id, localMatchingMTripletsPairs[j][1].m[0].id, localMatchingMTripletsPairs[j][1].m[1].id, localMatchingMTripletsPairs[j][1].m[2].id);
    }
    MTriplet mt0, mt1;
    int mmpcounter = 0;

    Serial.println("\nMinutiae pairs");
    for (int i = 0; i < mmtpSize; i++)
    {
        int containsMinutia1 = 0;
        int containsMinutia2 = 0;
        mt0 = localMatchingMTripletsPairs[i][0];
        mt1 = localMatchingMTripletsPairs[i][1];
        for (int j = 0; j < 3; j++)
        {
          containsMinutia1 = 0;
          containsMinutia2 = 0;
            for (int k = 0; k < mmpcounter; k++)
            {
                if (mt0.m[j].x == mmp[k][0].x && mt0.m[j].y == mmp[k][0].y)
                {
                    containsMinutia1 = 1;
                }
                if (mt1.m[j].x == mmp[k][1].x && mt1.m[j].y == mmp[k][1].y)
                {
                    containsMinutia2 = 1;
                }
            }
            if (!containsMinutia1 || !containsMinutia2)
            {
                printf("(%d,%d)(%d,%d)\n", mt0.m[j].x, mt0.m[j].y, mt1.m[j].x, mt1.m[j].y);
                mmp[mmpcounter][0] = mt0.m[j];
                mmp[mmpcounter][1] = mt1.m[j];
                mmpcounter++;
            }
        }
    }

    return mmpcounter;
}

/**
 * Global Minutiae Matching
 * This step uses every minutiae pair as a reference pair for fingerprint rotation and performs a query
 * minutiae transformation for each reference pair. Later, it selects the transformation that maximizes
 * the amount of matching minutiae.
 * Returns the maximum number of matching minutiae computed after performing all fingerprint rotations.
 */
int gmm(Minutia mmp[][2], int mmpsize)
{
    int n = 0;      // maximum number of matching minutiae
    Minutia bestE[mmpsize][2];
    Serial.println("\nGlobal Minutiae Matching");
    for (int i = 0; i < mmpsize; i++)
    {
        //printf("RefPair (%d,%d)(%d,%d)\n", mmp[i][0].x, mmp[i][0].y, mmp[i][1].x, mmp[i][1].y);
        Minutia E[mmpsize][2];
        int sizeE = 0;

        for (int j = 0; j < mmpsize; j++)
        {
            int contains = 0;
            for (int k = 0; k <= sizeE; k++)
            {
                if (((mmp[j][0].x == E[k][0].x) && (mmp[j][0].y == E[k][0].y)) || ((mmp[j][1].x == E[k][1].x) && (mmp[j][1].y == E[k][1].y)))
                {
                    contains = 1;
                }
            }
            if (contains == 0)
            {
                Minutia ql;
                int dAngle = mmp[i][1].o - mmp[i][0].o;
                int newAngle = mmp[j][0].o + dAngle;
                float seno = sin(dAngle / 57.2958);
                float coss = cos(dAngle / 57.2958);
                //printf("    Cosseno: %f", coss);
                ql.o = (newAngle > 360) ? newAngle - 360 : (newAngle < 0) ? newAngle + 360 : newAngle;
                ql.x = (mmp[j][0].x - mmp[i][0].x) * coss - (mmp[j][0].y - mmp[i][0].y) * seno + mmp[i][1].x;
                ql.y = (mmp[j][0].x - mmp[i][0].x) * seno + (mmp[j][0].y - mmp[i][0].y) * coss + mmp[i][1].y;
                //ql.x = cos(dAngle) * (mmp[j][0].x - mmp[i][0].x) - sin(dAngle) * (mmp[j][0].y - mmp[i][0].y) + mmp[i][1].x;
                //ql.y = sin(dAngle) * (mmp[j][0].x - mmp[i][0].x) + cos(dAngle) * (mmp[j][0].y - mmp[i][0].y) + mmp[i][1].y;
                //printf("    Pair (%d,%d)(%d,%d)\n", mmp[j][0].x, mmp[j][0].y, mmp[j][1].x, mmp[j][1].y);
                //printf("    mAngle %f\n", mmp[j][0].o);
                //printf("    dAngle %d\n", dAngle);
                //printf("    newAngle %d\n", newAngle);
                //printf("    Mapped minutia: (%d,%d)>%f\n", ql.x, ql.y, ql.o);
                //Serial.println("---------------------");
                if (fabs(ed(ql.x, ql.y, mmp[j][1].x, mmp[j][1].y)) <= tg && adpi(ad2pi(mmp[i][1].o, mmp[i][0].o), ad2pi(mmp[j][1].o, mmp[j][0].o)) <= ta && adpi(ql.o, mmp[j][1].o) <= ta)
                {
                    E[sizeE][0] = mmp[j][0];
                    E[sizeE][1] = mmp[j][1];
                    sizeE++;
                }
            }
        }
        if (n < sizeE)
        {
            n = sizeE;
            for (int w = 0; w < n; w++)
            {
              bestE[w][0] = E[w][0];
              bestE[w][1] = E[w][1];
            }

        }
    }
    printf("FINAL RESULT\n");
    for (int w = 0; w < n; w++)
    {
      printf("(%d,%d)(%d,%d)\n", bestE[w][0].x, bestE[w][0].y, bestE[w][1].x, bestE[w][1].y);
    }
    return n;
}

/**
 *  Similarity Score Computation
 */
float ssc(float n)
{
    return (n*n)/(qms_size * tms_size);
}

void initializeMinutiaeSets(Minutia qms[], Minutia tms[]) {
  
  float minutiae1[qms_size][3] = {
              {257,22,6.11520147323608},
              {282,46,3.33898830413818},
              {261,62,0.204244449734688},
              {288,56,0.610725939273834},
              {231,66,2.60117316246033},
              {233,79,0},
              {337,75,4.03764820098877},
              {233,90,3.14159274101257},
              {272,89,3.52209901809692},
              {313,85,3.68201208114624},
              {291,111,3.43304944038391},
              {340,100,3.43304944038391},
              {270,116,3.04192399978638},
              {274,126,6.18351650238037},
              {335,123,3.43304944038391},
              {260,140,3.24126124382019},
              {318,158,3.24126124382019},
              {259,176,2.94419717788696},
              {302,176,2.85013580322266},
              {324,191,2.85013580322266},
              {327,180,6.18351650238037},
              {337,199,6.08578968048096},
              {347,203,2.94419717788696},
              {308,277,2.94419717788696}
  };
              
  float minutiae2[tms_size][3] = {
              {351,158,3.60524034500122},
              {236,195,6.18351650238037},
              {292,205,3.24126124382019},
              {215,228,0},
              {249,245,3.14159274101257},
              {311,253,0},
              {316,270,3.27471876144409},
              {237,285,3.14159274101257},
              {318,282,0.0996686518192291},
              {345,280,0.560926795005798},
              {338,294,3.33898830413818},
              {252,304,3.04192399978638},
              {215,399,0.0996686518192291},
              {236,386,0.19739556312561},
              {253,396,0.291456788778305},
              {251,414,4.30172157287598},
              {304,414,3.59484052658081},
              {330,404,3.43304944038391},
              {227,428,0.19739556312561},
              {243,431,3.33898830413818},
              {335,421,3.33898830413818},
              {263,445,3.33898830413818},
              {275,441,0.0996686518192291}
  };

  int i;
  // Initialize query minutiae
  for(i=0; i<qms_size; i++) {
    qms[i].id = i;
    qms[i].x = minutiae1[i][0];
    qms[i].y = minutiae1[i][1];
    qms[i].o = minutiae1[i][2] * 57.2958;
  }
  
  // Initialize template minutiae
  for(i=0; i<tms_size; i++) {
    tms[i].id = i;
    tms[i].x = minutiae2[i][0];
    tms[i].y = minutiae2[i][1];
    tms[i].o = minutiae2[i][2] * 57.2958;
  }
}

int getTriangle1ValueAt(int line, int column) {
  int triangles1[num_triangles1][3] = {
    {21,19,20},
    {13,12,10},
    {5,7,4},
    {20,18,16},
    {22,21,19},
    {15,12,13},
    {19,18,20},
    {3,2,1},
    {10,12,8},
    {22,19,20},
    {22,21,20},
    {11,9,6},
    {5,4,2},
    {20,19,16},
    {19,18,16},
    {8,2,3},
    {13,12,8},
    {13,8,10},
    {7,4,2},
    {8,7,2},
    {7,5,2},
    {2,0,1},
    {8,7,5},
    {8,5,2},
    {10,8,9},
    {9,8,3},
    {21,19,18},
    {21,18,20},
    {15,12,10},
    {15,13,10},
    {14,9,11},
    {8,2,1},
    {8,1,3},
    {21,18,16},
    {21,20,16},
    {14,10,9},
    {1,3,0},
    {3,2,0},
    {8,4,2},
    {8,5,4},
    {8,7,4},
    {14,9,6},
    {11,14,6},
    {9,3,1},
    {14,10,11},
    {11,10,9},
    {2,4,0},
    {17,15,13},
    {12,15,8},
    {13,15,8},
    {22,18,20},
    {22,21,18},
    {22,19,18},
    {6,9,3},
    {10,12,9},
    {16,10,14},
    {8,12,2},
    {4,0,1},
    {2,4,1},
    {10,8,3},
    {9,10,3},
    {18,17,15},
    {13,9,10},
    {9,2,3},
    {18,17,13},
    {10,8,2},
    {20,16,14},
    {3,4,0},
    {4,3,2},
    {10,9,6},
    {11,10,6},
    {17,12,13},
    {17,15,12},
    {14,16,11},
    {16,18,14},
    {12,8,3},
    {18,17,12},
    {11,9,3},
    {6,11,3},
    {8,0,1},
    {19,16,14},
    {17,15,10},
    {14,16,9},
    {14,3,6},
    {23,21,22},
    {23,19,21},
    {23,19,22},
    {23,19,20},
    {23,20,21},
    {23,20,22}
  };
  return triangles1[line][column];
}

int getTriangle2ValueAt(int line, int column) {
  int triangles2[num_triangles2][3] = {
    {10,8,9},
    {19,18,15},
    {6,8,5},
    {9,8,6},
    {15,13,14},
    {20,16,17},
    {10,8,6},
    {10,6,9},
    {21,19,15},
    {22,21,19},
    {22,21,15},
    {22,19,15},
    {15,19,14},
    {14,12,13},
    {15,12,13},
    {15,12,14},
    {15,18,12},
    {21,18,19},
    {21,18,15},
    {15,18,14},
    {18,12,14},
    {19,18,12},
    {19,12,14},
    {18,13,14},
    {18,12,13},
    {15,18,13},
    {9,8,5},
    {9,6,5},
    {15,19,13},
    {19,13,14},
    {10,6,5},
    {10,8,5},
    {10,5,9},
    {22,18,19},
    {22,21,18},
    {22,21,16},
    {4,3,1},
    {22,15,16},
    {4,1,2},
    {11,7,4},
    {7,3,4},
    {5,4,2},
    {22,16,20},
    {22,19,16},
    {22,16,17},
    {22,17,20},
    {21,18,12},
    {5,6,2},
    {6,4,2},
    {8,11,6},
    {11,4,6},
    {21,22,20},
    {21,16,20},
    {16,14,17},
    {22,14,17},
    {8,11,4},
    {21,16,17},
    {20,21,17},
    {3,1,2},
    {4,3,2},
    {6,7,4},
    {11,7,6},
    {11,7,8},
    {11,7,3},
    {20,14,17},
    {4,7,1},
    {7,3,1},
    {5,1,2},
    {7,4,2},
    {7,1,2},
    {7,3,2},
    {5,2,0},
    {6,7,3},
    {6,1,2},
    {6,2,0},
    {6,5,0},
    {2,1,0},
    {5,1,0},
    {6,1,0}
  };
  return triangles2[line][column];
}

void setup() {
  Serial.begin(115200);
  
  Serial.print("Size of MTriplet ");
  Serial.println(sizeof(MTriplet));
  Serial.print("Size of Minutia ");
  Serial.println(sizeof(Minutia));
  Serial.print("Size of float ");
  Serial.println(sizeof(float));
  Serial.print("Size of uint16_t ");
  Serial.println(sizeof(uint16_t));
  
  Minutia qms[qms_size], tms[tms_size];
  
  initializeMinutiaeSets(qms, tms);
  
  Minutia* tempMinutia1 = (Minutia*) malloc(sizeof(Minutia));
  Minutia* tempMinutia2 = (Minutia*) malloc(sizeof(Minutia));
  Minutia* tempMinutia3 = (Minutia*) malloc(sizeof(Minutia));

  //Minutia tempMinutiaArray[3];
  // Initialize m-triplets
  firstNodeOfQueryMTripletLinkedList = initialize();
  firstNodeOfTemplateMTripletLinkedList = initialize();

  int i;
  for(i=0; i<num_triangles1; i++) {
    *tempMinutia1 = qms[getTriangle1ValueAt(i, 0)];
    *tempMinutia2 = qms[getTriangle1ValueAt(i, 1)];
    *tempMinutia3 = qms[getTriangle1ValueAt(i, 2)];
    Node* newNode = createMTriplet(*tempMinutia1, *tempMinutia2, *tempMinutia3);
    firstNodeOfQueryMTripletLinkedList = insert(firstNodeOfQueryMTripletLinkedList, newNode);
  }
  for(i=0; i<num_triangles2; i++) {
    *tempMinutia1 = tms[getTriangle2ValueAt(i, 0)];
    *tempMinutia2 = tms[getTriangle2ValueAt(i, 1)];
    *tempMinutia3 = tms[getTriangle2ValueAt(i, 2)];
    Node* newNode = createMTriplet(*tempMinutia1, *tempMinutia2, *tempMinutia3);
    firstNodeOfTemplateMTripletLinkedList = insert(firstNodeOfTemplateMTripletLinkedList, newNode);
  }

  // 3 - Set euclidean distances
  Node* auxNode = (Node*) malloc(sizeof(Node));
  auxNode = firstNodeOfQueryMTripletLinkedList;
  Serial.println("\nQuery Euclidean distances\n");
  while (auxNode->next != NULL) {
    auxNode = auxNode->next;
    MTriplet* queryTriplet = (MTriplet*) auxNode->info;
    queryTriplet->d[0] = ed(queryTriplet->m[0].x, queryTriplet->m[0].y, queryTriplet->m[1].x, queryTriplet->m[1].y);
    queryTriplet->d[1] = ed(queryTriplet->m[1].x, queryTriplet->m[1].y, queryTriplet->m[2].x, queryTriplet->m[2].y);
    queryTriplet->d[2] = ed(queryTriplet->m[2].x, queryTriplet->m[2].y, queryTriplet->m[0].x, queryTriplet->m[0].y);
    /*printf("%d %d %d\n", queryTriplet->m[0].id, queryTriplet->m[1].id, queryTriplet->m[2].id);
    Serial.print(" d1:");
    Serial.print(queryTriplet->d[0]);
    Serial.print(" d2:");
    Serial.print(queryTriplet->d[1]);
    Serial.print(" d3:");
    Serial.println(queryTriplet->d[2]);*/
  }
  auxNode = firstNodeOfTemplateMTripletLinkedList;
  Serial.println("\nTemplate Euclidean distances\n");
  while (auxNode->next != NULL) {
    auxNode = auxNode->next;
    MTriplet* templateTriplet = (MTriplet*) auxNode->info;
    templateTriplet->d[0] = ed(templateTriplet->m[0].x, templateTriplet->m[0].y, templateTriplet->m[1].x, templateTriplet->m[1].y);
    templateTriplet->d[1] = ed(templateTriplet->m[1].x, templateTriplet->m[1].y, templateTriplet->m[2].x, templateTriplet->m[2].y);
    templateTriplet->d[2] = ed(templateTriplet->m[2].x, templateTriplet->m[2].y, templateTriplet->m[0].x, templateTriplet->m[0].y);
    /*printf("%d %d %d\n", templateTriplet->m[0].id, templateTriplet->m[1].id, templateTriplet->m[2].id);
    Serial.print(" d1:");
    Serial.print(templateTriplet->d[0]);
    Serial.print(" d2:");
    Serial.print(templateTriplet->d[1]);
    Serial.print(" d3:");
    Serial.println(templateTriplet->d[2]);*/
  }
  
  // 4 - Calculate alphas
  auxNode = firstNodeOfQueryMTripletLinkedList;
  Serial.println("\nQuery Triplet Alphas\n");
  while (auxNode->next != NULL) {
    auxNode = auxNode->next;
    MTriplet* queryTriplet = (MTriplet*) auxNode->info;
    calculateAlphas(queryTriplet);
  }
  auxNode = firstNodeOfTemplateMTripletLinkedList;
  Serial.println("\nTemplate Triplet Alphas\n");
  while (auxNode->next != NULL) {
    auxNode = auxNode->next;
    MTriplet* templateTriplet = (MTriplet*) auxNode->info;
    calculateAlphas(templateTriplet);
  }
  
  // 5 - Calculate betas
  Serial.println("\nQuery Betas\n");
  auxNode = firstNodeOfQueryMTripletLinkedList;
  while (auxNode->next != NULL) {
    auxNode = auxNode->next;
    MTriplet* queryTriplet = (MTriplet*) auxNode->info;
    calculateBetas(queryTriplet);
  }
  Serial.println("\nTemplate Betas\n");
  auxNode = firstNodeOfTemplateMTripletLinkedList;
  while (auxNode->next != NULL) {
    auxNode = auxNode->next;
    MTriplet* templateTriplet = (MTriplet*) auxNode->info;
    calculateBetas(templateTriplet);
    //printf("%d,%d,%d\n", templateTriplet->b[0], templateTriplet->b[1], templateTriplet->b[2]);
  }

  Minutia mmp[10][2];//TODO tirar tamanho fixo
  int matchingPairs = lmm(qms, tms, mmp);
  Serial.print("Matching Pairs: ");
  Serial.println(matchingPairs);
  int n = gmm(mmp, matchingPairs);
  Serial.print("Matching minutiae: ");
  Serial.println(n);
  float similarity = ssc(n);
  Serial.print("Similarity: ");
  Serial.println(similarity);
  Serial.println("Finished");
}

void loop() {
  Serial.println(".");
  delay(5000);
}
