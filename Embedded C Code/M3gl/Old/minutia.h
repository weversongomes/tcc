#ifndef _minutia_h
#define _minutia_h

typedef struct minutia
{
    uint16_t id;
    uint16_t x, y;              // position
    float o;                    // minutia direction
};
typedef struct minutia Minutia;

#endif
