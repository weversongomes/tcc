% -------------------------------------------------------------------------
% Esta funcao extrai as miuncias (pontos finais e bifurcacoes de uma
% impressao digital. O retorno da-se em coordenadas X e Y das minucias
% Autora: Cristina Monica, orientanda do Prof.Adilson Gonzaga
% Bibliografia: THAI, Raymond. Fingerprint Image Enhancement and Minutiae Extraction. 
% -------------------------------------------------------------------------

function Minu = Extrai_minucia()

% read image into memory
% img = imread(Arq);
img = imread('DB1_B/101_3.tif');

% convert to gray scale and thin image at same time
% thinned = edge(rgb2gray(img), 'canny');
thinned = edge(img, 'canny');

% minutiae counter
minu_count = 1;
minutiae(minu_count, :) = [0,0,0,0];

% loop through image and find minutiae, ignore 10 pixels for border
for y=10:size(img, 2) - 10
    for x=10:size(img,1) - 10
        if (thinned(x, y) == 1) % only continue if pixel is white
            % calculate CN from Raymond Thai
            CN = 0;
            for i = 1:8
                CN = CN + abs (p(thinned, x, y, i) - p(thinned, x, y, i + 1));
            end   
            CN = CN / 2;
            
            if (CN == 1) | (CN == 3)
                theta = FindTheta(thinned, x, y, CN);
                minutiae(minu_count, :) = [x, y, CN, theta];
                minu_count = minu_count + 1;
            end
        end % if pixel white
    end % for y
end % for x

% make minutiae image
minutiae_img = uint8(zeros(size(img, 1),size(img, 2), 3));
for i=1:minu_count - 1
    x1 = minutiae(i, 1);
    y1 = minutiae(i, 2);
    if (minutiae(i, 3) == 1) 
        minutiae_img(x1, y1,:) = [255, 0, 0]; % red for ridge endings
    else
        minutiae_img(x1, y1,:) = [0, 0, 255]; % blue for ridge bijections
    end

%********IGNORE FOLLOWING FOR NOW*************************
%    % cos and sin in radians we have theta in degrees so convert
%    
%    x2 = round(x1 + 3  * cos(minutiae(1,4)));
%    y2 = round(y1 + 3  * sin(minutiae(1,4)));
%
%    % make sure does not go outside of boundary
%    if (x2 < 0) 
%        x2 = 0;
%    end
%    if (x2 > size(minutiae_img, 2))
%        x2 = size(minutiae_img, 2);
%    end
%    if (y2 < 0)
%        y2 = 0;
%    end
%    if (y2 > size(minutiae_img, 1))
%        y2 = size(minutiae_img, 1);
%    end
%   minutiae_img = func_DrawLine(minutiae_img, x1, y1, x2, y2, 1);
%*********END IGNORE******************************************
end % for loop through minu_count

% merge thinned image and minutia_img together
combined = uint8(minutiae_img);
for x=1:size(thinned)
    for y=1:size(thinned)
        if (thinned(x,y))
            combined(x,y,:) = [255,255,255];
        else
            combined(x,y,:) = [0,0,0];
        end % end if
        if ((minutiae_img(x,y,3) ~= 0) | (minutiae_img(x,y,1) ~= 0))
            combined(x,y,:) = minutiae_img(x,y,:);
        end
        
    end % end for y
end % end for x

% Retorna as coordenadas x e y das minucias
Minu = minutiae(:,:);

dt = delaunayTriangulation(minutiae(1:end,1), minutiae(1:end,2));
triplot(dt);

triangles = uint8(minutiae_img);
for x=1:size(thinned)
    for y=1:size(thinned)
        if (thinned(x,y))
            combined(x,y,:) = [255,255,255];
        else
            combined(x,y,:) = [0,0,0];
        end % end if
        if ((minutiae_img(x,y,3) ~= 0) | (minutiae_img(x,y,1) ~= 0))
            combined(x,y,:) = minutiae_img(x,y,:);
        end
        
    end % end for y
end % end for x
% Plota imagens
% subplot(2,2,1), subimage(img), title('orignal image');
% subplot(2,2,2), subimage(combined), title('thinned and minutiae points');
% subplot(2,2,2), subimage(thinned), title('thinned image')
% subplot(2,2,3), subimage(minutiae_img), title('minutiae points')