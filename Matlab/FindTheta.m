function theta = FindTheta(img, x, y, CN)
theta = -1;

if (CN == 1)
    % for ridge ending find the only other white pixel
    for i=1:8
        if (p(img, x, y, i) == 1)
            % found only other pixel decide which angle
            switch (i)
                case {1, 9}
                    theta = 0;
                case 2
                    theta = 45;
                case 3
                    theta = 90;
                case 4
                    theta = 135;
                case 5
                    theta = 180;
                case 6
                    theta = 225;
                case 7
                    theta = 270;
                case 8
                    theta = 315;
            end % switch
        end % if Pxyi = 1
    end % for i
else 
    % ridge bijection, find single pixel on an axis
    if (p(img, x, y, 1) & ~(p(img, x, y, 2) & p(img, x, y, 8)) ) 
        theta = 0;
    end
    if (p(img, x, y, 2) & ~(p(img, x, y, 1) & p(img, x, y, 8) & p(img, x, y, 3) & p(img, x, y, 4)))
        theta = 45;
    end
    if (p(img, x, y, 3)& ~(p(img, x, y, 2) & p(img, x, y, 4)))
        theta = 90;
    end
    if (p(img, x, y, 4)& ~ (p(img, x, y, 3) & p(img, x, y, 2) & p(img, x, y, 5) & p(img, x, y, 6)))
        theta = 135;
    end
    if (p(img, x, y, 5) & ~ (p(img, x, y, 4) & p(img, x, y, 6)))
        theta = 180;
    end
    if (p(img, x, y, 6) & ~ (p(img, x, y, 4) & p(img, x, y, 5) & p(img, x, y, 7) & p(img, x, y, 8)))
        theta = 225;
    end
    if (p(img, x, y, 7) & ~ (p(img, x, y, 6) & p(img, x, y, 8)))
        theta = 270;
    end
    if (p(img, x, y, 8) & ~ (p(img, x, y, 2) & p(img, x, y, 1) & p(img, x, y, 7) & p(img, x, y, 6)))
        theta = 315;
    end
end