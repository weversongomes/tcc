% -------------------------------------------------------------------------
% Esta funcao salva em PF as coordenadas X e Y dos pontos finais
% -------------------------------------------------------------------------

function PF = AchaPF(Temp)

% Recebe os indices 
Idx = find(Temp(:,3) == 1);

Tam = size(Idx);

for i = 1:Tam(1,1)
    PF(i,:) = Temp(Idx(i,1),1:2);
end