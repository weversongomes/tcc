% -------------------------------------------------------------------------
% Esta funcao salva em BI as coordenadas X e Y das bifurcacoes
% -------------------------------------------------------------------------

function BI = AchaBI(Temp)

% Recebe os indices 
Idx = find(Temp(:,3) == 3);

Tam = size(Idx);

for i = 1:Tam(1,1)
    BI(i,:) = Temp(Idx(i,1),1:2);
end